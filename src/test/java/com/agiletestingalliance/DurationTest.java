package com.agiletestingalliance;

import static org.junit.Assert.*;

import org.junit.Test;

public class DurationTest {

	@Test
	public void durationTest() {
		Duration a = new Duration();
		String cadenaActual = a.dur();
		String cadenaEsperada = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and cant dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
		assertEquals(cadenaEsperada, cadenaActual);
	}

}
